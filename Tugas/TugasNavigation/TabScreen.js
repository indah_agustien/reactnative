import React, {Component} from 'react';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';
import SkillScreen from './SkillScreen';


import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

export default function TabScreen() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="ProjectScreen" component={ProjectScreen} />
      <Tab.Screen name="AddScreen" component={AddScreen} />
      <Tab.Screen name="SkillScreen" component={SkillScreen} />
    </Tab.Navigator>
  );
}
