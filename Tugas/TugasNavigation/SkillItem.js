import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
export default class SkillItem extends Component {
  

  render() {
     
   
    let skill = this.props.skill;
  
    return (
        <View style={styles.container}>
            <ImageBackground style={styles.bcgBottom}>
            <Icon name="react" size={100} color={"#003366"}  />
           

            <View style={styles.desContainer}>
               
                <Text style={styles.stat1}>{skill.skillName}</Text>
                
            </View>
            <View style={styles.stat4}>
            <Icon name="chevron-right" size={100} color={"#003366"} 
         
     />
     </View>
          
            <View>
            <Text style={styles.stat2}>{skill.categoryName}</Text>
            </View>
          
            <View>
         
            <Text style={styles.stat3}>{skill.percentageProgress}</Text>
           
            </View>
           
            </ImageBackground>
           
           
        </View>
        
    )
}
}


const styles = StyleSheet.create({
container: {
  flex: 1,
},
desContainer: {
    flexDirection: 'row',
      
        marginTop: 60,
    
},
stat1: {
    fontSize: 24,
    marginTop: -150,
    marginLeft: 150,
    color:"#003366"

},
stat2: {
    fontSize: 16,
    marginTop: -70,
    marginLeft: 150,
    color:"#3EC6FF"

},
stat3: {
    fontSize: 48,
    marginTop: -50,
    marginLeft: 185,
    color: "#ffffff"


},
stat4: {
    marginLeft: 290,
    marginTop: -150
},
bcgBottom: {
    backgroundColor: "#B4E9FF",
    height: 150,
    width: 375,
    marginLeft: -372,
    borderRadius: 16,
    marginTop: 50

  }

})