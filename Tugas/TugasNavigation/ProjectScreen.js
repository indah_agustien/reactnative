import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class ProjectScreen extends Component {
  

  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontSize:20, fontWeight:"bold"}}> Halaman Proyek </Text>
      </View>
    );
  }
}

const styles=StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
})
