import React, {Component} from 'react';

import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';

import { createDrawerNavigator, DrawerItemS } from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';

import LoginScreen from './LoginScreen';
import TabScreen from './TabScreen';
import AboutScreen from './AboutScreen';

const Drawer = createDrawerNavigator(
  contentComponent= CustomDrawerComponent
);

export default function App () {

    return(
      <NavigationContainer>
      <Drawer.Navigator initialRouteName="LoginScreen">
    
      <Drawer.Screen name=" " component={LoginScreen}/>
                                      
        <Drawer.Screen name="TabScreen" component={TabScreen} />
        <Drawer.Screen name="AboutScreen" component={AboutScreen}/>
       
      </Drawer.Navigator>
    </NavigationContainer>
    );
  }

const CustomDrawerComponent = (props) => (
  <SafeAreaView style={{flex:1}}>
    
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
)


  
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center"

  }
})