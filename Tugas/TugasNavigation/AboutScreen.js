import React, { Component } from 'react';
 import { View, Text, StyleSheet,Image, ImageBackground} from 'react-native';
// import { NavigationContainer } from '@react-navigation/native'; ==> belum paham
// import { createStackNavigator } from '@react-navigation/stack'; ==> belum paham

import Icon from 'react-native-vector-icons/FontAwesome';
export default class AboutScreen extends Component {
  
  render() {
  
    return (
      <View>
        <Text style={styles.txt2}> Tentang Saya </Text>
        <Image source={require('./assets/pictindah.png')} style={{width: 200,height: 200,
         borderRadius: 40,
         marginLeft: 110,
         marginTop: 10
         }}/>
         <View>
             <Text style={styles.txtIndah}>Indah Agustien</Text>
             <Text style={styles.txtReact}>React Native Developer</Text>
         </View>
         <View>
             <ImageBackground style={styles.bcg}>
                <View >
                    <Text style={styles.txtPort} >Portfolio </Text>
                 </View>
                 <View style={styles.navBar}>
                 <Icon name = "gitlab" size={35} color="#3EC6FF" />
                    <Text style={styles.txtPort1} >@indah_agustien </Text>
                    <Icon name = "github" size={35} color="#3EC6FF" />
                    <Text style={styles.txtPort1} >@indah_007 </Text>
                 </View>
             </ImageBackground>
         </View>
         <View>
           <ImageBackground style={styles.bcgBottom}>
           <View >
                    <Text style={styles.txtPort} >Hubungi Saya </Text>
                 </View>
                 <View style={styles.navBar}>
                   <Icon name="facebook" size={25}  color="#3EC6FF" />
                   <Text style={{ color:"#003366", fontWeight:"bold", fontSize:16}}>indah_FB</Text>
                 </View>
                 <View style={styles.navBar}>
                   <Icon name="instagram" size={25}  color="#3EC6FF" />
                   <Text style={{ color:"#003366", fontWeight:"bold", fontSize:16}}>@indah_IG</Text>
                 </View>
                 <View style={styles.navBar}>
                   <Icon name="twitter" size={25}  color="#3EC6FF"  />
                   <Text style={{ color:"#003366", fontWeight:"bold", fontSize:16}}>@indah_twitter</Text>
                 </View>
           </ImageBackground>
         </View>
        
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
    txt2: {
        fontSize: 36,
        color: "#003366",
        marginLeft: 95,
        marginTop: 64,
        fontWeight: "bold"

    },
    txtIndah: {
        fontSize: 24,
        fontWeight: "bold",
        color: "#003366",
        marginLeft: 125,
        marginTop: 15
    },
    txtReact: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#3EC6FF",
        marginLeft: 125,
    },
    bcg: {
        backgroundColor: "#EFEFEF",
        height: 100,
        width: 395,
        marginLeft: 8,
        borderRadius: 16,
        marginTop: 15
    
    },
    txtPort: {
        fontSize: 18,
        color: "#003366",
        marginLeft: 5,
        marginTop: 5,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    txtPort1: {
      fontSize: 18,
        color: "#003366",
        marginLeft: 5,
        marginTop: 5,
        fontWeight: "bold"
    },
    navBar: {
      justifyContent: "space-around",
      flexDirection: "row",
      marginTop: 15
    },
    
    bcgBottom: {
      backgroundColor: "#EFEFEF",
      height: 200,
      width: 395,
      marginLeft: 8,
      borderRadius: 16,
      marginTop: 15

    }
})


