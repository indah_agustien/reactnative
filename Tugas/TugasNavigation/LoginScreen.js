import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';

export default class LoginScreen extends Component {
  
  render() {
    return (
      <View style={styles.container}>
       <View style={styles.navBar}>
       <Image style={styles.logoSanber} source={require('./assets/logo.png')} />
       <Text style={styles.txt1}>Login</Text>
       </View>
       <View style={styles.inputContainer}>
           <TextInput style={styles.input1} placeholder={'Username/Email'}
           placeholderTextColor={'rgba(44, 130, 201, 1)'}
           underlineColorAndroid={'#000000' } 
           />
            <TextInput style={styles.input1} placeholder={'Password'}
            secureTextEntry={true}
            placeholderTextColor={'rgba(44, 130, 201, 1)'}
          
           underlineColorAndroid={'#000000' } 
           />
       </View>
       <View>
           <TouchableOpacity style={styles.buttonMasuk}>
            <Text style={styles.textMasuk} >MASUK</Text>
           </TouchableOpacity>
       </View>
       <View>
           <Text style={styles.atau}>Atau</Text>
       </View>
       <View>
       <TouchableOpacity style={styles.buttonDAFTAR}>
            <Text style={styles.textMasuk} >DAFTAR ?</Text>
           </TouchableOpacity>
       </View>
      

      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    navBar: {

    },
    txt1: {
        fontSize: 24,
        color: "#003366",
        position: "absolute",
        width: 60,
        height: 28,
        marginTop: 235,
        marginLeft: 190,
        lineHeight: 28
    },
    logoSanber: {
        position: "absolute",
        width: 375,
        height: 102,
        marginLeft: 20,
        marginTop: 63


    },
    inputContainer: {
        marginTop: 326,
        marginLeft: 20,
        marginRight: 20
    },
    input1: {
        height: 45,
    borderRadius: 45,
    marginLeft: 10,
    fontSize: 16
   
    },
    buttonMasuk: {
        backgroundColor: "#3EC6FF",
        borderRadius: 16,
        width: 140,
        height: 40,
        marginLeft: 152,
        marginTop: 80

    },
    buttonDAFTAR: {
        backgroundColor: "#003366",
        borderRadius: 16,
        width: 140,
        height: 40,
        marginLeft: 152,
        marginTop: 30

    },
    textMasuk: {
        fontSize: 24,
        lineHeight: 28,
        color: "#FFFFFF",
        textAlign: "center",
        paddingTop: 5
       
        
    },
    atau: {
        fontSize: 24,
        color: "#3EC6FF",
        marginLeft: 200,
        marginTop: 30,
        lineHeight: 28
    }
})
