import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, FlatList, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SkillItem from './SkillItem';
import skillData from './skillData.json';




export default class SkillScreen extends Component {

  render() {
    
    
    return (
      <View style={styles.container}>
          <View style={styles.navBar} >
          <Image style={styles.logoSanber} source={require('./images/logo.png')} />
          </View>
          <View style={{flexDirection:'row', marginLeft: 10}}>
            <Icon name="account-circle" size={25} color="#3EC6FF" />
            <View style={{flexDirection:'column', marginLeft: 10}}>
            <Text style={{fontSize:12, color:"#000000"}}>Hai,</Text>
            <Text style={{fontSize:16, color:"#003366"}}>Indah Agustien</Text>
            <Text style={{fontSize: 36, color:"#003366", 
            marginLeft:-20, marginTop: 5 , borderBottomColor: '#3EC6FF',
            borderBottomWidth: 4, borderEndWidth: 275}}>SKILL</Text>
            </View>
           
          </View>
          <View style={{flexDirection: "row", marginLeft: 12}}>
                <ImageBackground style={styles.header1} >
                 <Text style={{textAlign:"center", fontSize: 12}}>Library / Framework</Text>
                </ImageBackground>
                <ImageBackground style={styles.header1} >
                 <Text style={{textAlign:"center", fontSize: 12}}>Bahasa Pemrograman</Text>
                </ImageBackground>
                <ImageBackground style={styles.header2} >
                 <Text style={{textAlign:"center", fontSize: 12}}>Teknologi</Text>
                </ImageBackground>
                
                <View style={styles.body}>
               <SkillItem  skill={skillData.items[0]} />
              
               
        
        </View>
            </View>
           
      </View>
    );
  }
}

const styles= StyleSheet.create({
    container: {
        flex: 1,
    },
    navBar: {

    },
    logoSanber: {
        height: 51,
        width: 187,
        marginLeft: 195

    },
    header1: {
        backgroundColor: "#B4E9FF",
        height: 20,
        width: 135,
        marginTop: 10,
        marginLeft: 10,
        borderRadius: 8
    },
    header2: {
        backgroundColor: "#B4E9FF",
        height: 20,
        width: 80,
        marginTop: 10,
        marginLeft: 10,
        borderRadius: 8
    },
    body: {
        flex: 1
    }
})
