/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './Tugas/TugasNavigation/App';
import {name as appName} from './app.json';


AppRegistry.registerComponent(appName, () => App);
